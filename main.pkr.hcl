source "amazon-ebs" "nginx" {
  ami_name      = "nginx-{{timestamp}}"
  instance_type = "t2.micro"
  ssh_username  = "ubuntu"
  source_ami_filter {
    filters = {
#     name                = "ubuntu/images/*ubuntu-xenial-20.04-amd64-server-*"
      name                = "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
#   owners      = ["965806849863"]
    owners = ["637423400451", "amazon"] # my account owner id

  }
}


build {
  sources = [
    "source.amazon-ebs.nginx"
  ]

  provisioner "ansible" {
    galaxy_file = "./requirements.yml"
    playbook_file = "./playbook.yaml"
  }

  post-processors {
    post-processor "artifice" {
      files = ["nginx.json"]
    }
  }

}